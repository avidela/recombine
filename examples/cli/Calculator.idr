module Calculator

import Server
import Server.CLI
import Server.EDSL.Lens
import Server.EDSL.Path

div, min, mul, add : Int +/ Int +/ Unit -> Int
div (MkPathExt a (MkPathExt b ())) = Prelude.div a b
min (MkPathExt a (MkPathExt b ())) = a - b
mul (MkPathExt a (MkPathExt b ())) = a * b
add (MkPathExt a (MkPathExt b ())) = a + b

public export
calculator : DepServer ? ? ?
calculator = ("add" / Pure add
          &&& "div" / Pure div
          &&& "mul" / Pure mul
          &&& "min" / Pure min)

main : IO ()
main = runServer calculator Normal def

