module IOT

import Server
import Server.CLI
import Server.EDSL.Lens

%hide Prelude.(*)
%hide Prelude.(&&)
%hide Prelude.(/)

-- The type of the server state
public export
HomeState : Type
HomeState = Bool * (Bool * Bool * Bool)

-- the server's initial state
initialState : HomeState
initialState = False && (False && False && False)

-- Get the boiler out of the state
boilerLens : Lens (Dup HomeState) (Dup Bool)
boilerLens = fstLens

-- Get the lights out of the state
lightsLens : Lens (Dup HomeState) (Dup (Bool * Bool * Bool))
lightsLens = sndLens

-- A way to interact with the state of the server
serverStateLens : Para BUnit (Dup HomeState) (Dup HomeState)
serverStateLens = stateLens HomeState

serverLightsLens : Para BUnit (Dup HomeState) (Dup (Bool * Bool * Bool))
serverLightsLens = postCompose serverStateLens lightsLens

serverBoilerLens : Para BUnit (Dup HomeState) (Dup Bool)
serverBoilerLens = postCompose serverStateLens boilerLens

kitchenLight : Para BUnit (Dup HomeState) (Dup Bool)
kitchenLight = (serverLightsLens `postCompose` fstLens) -- `postCompose` fstLens

bedroomLight : Para BUnit (Dup HomeState) (Dup Bool)
bedroomLight = (serverLightsLens `postCompose` sndLens) `postCompose` fstLens

livingroomLight : Para BUnit (Dup HomeState) (Dup Bool)
livingroomLight = (serverLightsLens `postCompose` sndLens) `postCompose` sndLens

public export
iotServer : DepServer ? ? ?
iotServer = "lights" / ("living" / Lens livingroomLight
                       &&&
                       "bedroom" / Lens bedroomLight
                       &&&
                       "kitchen" / Lens kitchenLight)
             &&&
             "boiler" / Lens serverBoilerLens

main : IO ()
main = runServer
         -- The server
         iotServer
         -- The log level
         Normal
         -- The initial state. `def` stands for "default value"
         def

