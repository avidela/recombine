module Interfaces

import Data.Vect
import Data.List
import Data.SortedMap
import Data.Product
import Data.Sum
import Data.Carrier

import Server.Path

%hide Prelude.(+)

||| Obtain a string representation of a type
public export
interface Display t where
  constructor MkDisplay
  display : String

||| Obtain a string representation of an indexed type
public export
interface IDisplay idx (0 t : idx -> Type) where
  constructor MkIDisplay
  iDisplay : idx -> String

export
Display t => Display (Carrier t) where
  display = "Carrier \{display {t}}"

export
Display t => IDisplay s (const t) where
  iDisplay _ = display {t}

%hint
export
funDisplay : {f : q -> a} -> (i : IDisplay a b) => IDisplay q (b . f)
funDisplay = MkIDisplay $ \arg => (iDisplay (f arg) {t=b})

export
Display Bool where
  display = "Bool"

export
Display Int where
  display = "Int"

export
Display Double where
  display = "Double"

export
Display String where
  display = "String"

export
Display a => Display b => Display (Pair a b) where
  display = "(\{display {t=a}}, \{display {t=b}})"

export
Display a => Display b => Display (a * b) where
  display = "\{display {t=a}} * \{display {t=b}}"

export
(ad : Display a) => (bd : Display b) => Display (a +/ b) where
  display = "\{display {t=a}} / \{display {t=b}}"

export
Display a => Display b => Display (a + b) where
  display = "\{display {t=a}} + \{display {t=b}}"

export
Display a => Display (List a) where
  display = "List \{display {t=a}}"

export
Display Nat where
  display = "Nat"

export
Display Builtin.Unit where
  display = "()"

export
Display a => Display (Vect n a) where
  display = "Vect n \{display{t=a}}"

export
Display key => Display val => Display (SortedMap key val) where
  display = "SortedMap \{display {t=key}} \{display {t=val}}"

||| The default interface allows to obtain a default value for the given type
||| It also has a `defs` function that returns multiple other default values.
||| By default (no pun intended) the `defs` function return a list with a single
||| value: The one from the function `def`.
public export
interface Default t where
  constructor MkDefault
  def : t
  defs : List t
  defs = [def]

public export
interface IDefault idx (0 t : idx -> Type) where
  constructor MkIDefault
  iDef : {v : idx} -> t v
  iDefs : {v : idx} -> List (t v)

export
Default t => Default (Carrier t) where
  def = Carry def
  defs = Carry <$> defs

export
Default t => IDefault s (const t) where
  iDef = def
  iDefs = defs

%hint
export
funDefault : {f : q -> a} -> (i : IDefault a b) => IDefault q (b . f)
funDefault = MkIDefault (iDef {t=b}) (iDefs {t=b})

export
Default a => Default b => Default (Pair a b) where
  def = (def, def)
  defs = [(def1, def2) | def1 <- defs {t=a}, def2 <- defs {t=b}]

export
Default a => Default b => Default (a * b) where
  def = (def && def)
  defs = [(def1 && def2) | def1 <- defs {t=a}, def2 <- defs {t=b}]

export
Default a => Default b => Default (a +/ b) where
  def = (MkPathExt def def)
  defs = [(MkPathExt def1 def2) | def1 <- defs {t=a}, def2 <- defs {t=b}]

export
Default a => Default b => Default (a + b) where
  def = (<+ def)
  defs = [ (<+ def1) | def1 <- defs {t=a}] ++ [ (+> def2) | def2 <- defs {t=b}]

export
Default a => Default (List a) where
  def = defs {t=a} ++ defs {t=a}
  defs = [[], def]

export
Default Double where
  def = 0
  defs = [0, 3.14, -9.8]

export
Num n => Default n where
  def = 7
  defs = [7, 1, -3]

export
Default () where
  def = ()

export
Default Bool where
  def = True
  defs = [True, False]

export
Default String where
  def = "hello"
  defs = ["hello", "lorem ipsum dolor", ""]

public export
interface IShow idx (0 t : idx -> Type) where
  constructor MkIShow
  iShow : {v : idx} -> t v -> String

%hint
export
basicIShow : Show v => IShow s (const v)
basicIShow = MkIShow show

%hint
export
funShow : {f : q -> a} -> (s : IShow a b) => IShow q (b . f)
funShow = MkIShow (iShow {t=b})

%hint
export
ishowSumDep : IShow p q => IShow p' q' => IShow (p + p') (Sum.choice q q')
ishowSumDep = MkIShow showHelp
  where
    showHelp : {v : p + p'} -> Sum.choice q q' v -> String
    showHelp val {v = (<+ x)} = iShow {idx = p} val
    showHelp val {v = (+> x)} = iShow {idx = p'} val

export
[ishowSum] Show a => Show b => IShow (a + b) (Sum.choice (const a) (const b)) where
  iShow sad = case v of
                   (<+ x) => show x
                   (+> x) => show x

export
[ishowTy] Show a => (p : IShow b q) => IShow (a +/ b) (\x => Product.(*) a (q x.tail)) where
  iShow arg = case v of
                   (MkPathExt v1 v2) => let
                     s1 = show arg.fst
                     s2 = iShow {idx = b} {t=q} arg.snd in
                     "\{s1} && \{s2}"

export
[ishowTensorChoice] (p1 : IShow p q) => (p2 : IShow p' q') => IShow (p * p') (\st => Sum.(+) (q (st.fst)) (q' (st.snd))) where
  iShow (<+ x) {v = v1 && v2} = iShow {idx = p} x
  iShow (+> x) {v = v1 && v2} = iShow {idx = p'} x

export
[ishowTensorProd] (p1 : IShow p q) => (p2 : IShow p' q') => IShow (p * p') (\st => Product.(*) (q (st.fst)) (q' (st.snd))) where
  iShow (x && y) {v = v1 && v2} = iShow {idx = p} x ++ " && " ++ iShow {idx = p'} y

export
IDisplay (() + ()) (Sum.choice (const ()) (const ())) where
  iDisplay _ = "()"

export
[idisplayTy] Display a => (p : IDisplay b q) =>
             IDisplay (a +/ b) (\x => ty * q x.tail) where
  iDisplay index = case index of
                   (MkPathExt v1 v2) => let
                     v1 = display {t=a}
                     v2 = iDisplay {idx = b} {t=q} v2 in
                     "\{v1} && \{v2}"

export
[idisplayTensorSum] (p1 : IDisplay p q) => (p2 : IDisplay p' q') => IDisplay (p * p') (\st => Sum.(+) (q (st.fst)) (q' (st.snd))) where
  iDisplay (v1 && v2) = iDisplay {t=q} v1 ++ " + " ++ iDisplay {t=q'} v2

export
[idisplayTensorProd] (p1 : IDisplay p q) => (p2 : IDisplay p' q') => IDisplay (p * p') (\x => q (x .fst) * q' (x .snd)) where
  iDisplay (v1 && v2) = iDisplay {t=q} v1 ++ " * " ++ iDisplay {t=q'} v2

export
[idisplaySum] (p1 : IDisplay p q) => (p2 : IDisplay p' q') => IDisplay (p + p') (Sum.choice q q') where
  iDisplay (<+ x) = iDisplay {t=q} x
  iDisplay (+> x) = iDisplay {t=q'} x

export
[idefaultSum] IDefault p q => IDefault p' q' => IDefault (p + p') (Sum.choice q q') where
  iDef {v = (<+ x)} = iDef {idx=p}
  iDef {v = (+> x)} = iDef {idx=p'}
  iDefs {v = (<+ x)} = iDefs {idx=p}
  iDefs {v = (+> x)} = iDefs {idx=p'}

export
[idefaultProd] IDefault p q => IDefault p' q' => IDefault (p * p') (\x => Product.(*) (q x.π1) (q' x.π2)) where
  iDef {v = (fst && snd)} = iDef {idx=p} && iDef {idx=p'}
  iDefs {v} = map fromPair $ zip {z=List} (iDefs {idx=p}) (iDefs {idx = p'})

export
[idefaultTy] Default a => (p : IDefault b q) =>
             IDefault (a +/ b) (\x => Product.(*) a (q x.tail)) where
  iDef {v} = case v of
                  (MkPathExt v1 v2) => def {t=a} && iDef {v=v2}
  iDefs {v} = case v of
                   (MkPathExt v1 v2) =>
                     let ds = defs {t=a}
                         ds' = iDefs {v=v2} {idx=b} {t=q} in zipWith (&&) ds ds'

||| A Type is documented if it has instance for `Show` `Display` and `Default`
public export
Documented : Type -> Type
Documented t = Show t * Display t * Default t

||| A Type is documented if it has instance for `IShow` `IDisplay` and `IDefault`
export
interface IShow i t => IDisplay i t => IDefault i t => IDocumented i t where


||| There is no implementation necessary, it's like a type-alias
export
implementation IShow i t => IDisplay i t => IDefault i t => IDocumented i t where
