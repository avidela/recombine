module Server.CLI

import Data.Container
import Server

||| Given a server implementation as a `ServerInstance` start listening for requests and respond to them.
||| This function requires an initial state that will be changed in memory
partial export
runServer : {0 c : Container} -> ServerInstance api c =>
            api -> LogLevel -> (initial : c.shp) -> IO ()
runServer api logLevel initial =
  let requestHandler = toHandler api
  in runLog {lvl = logLevel} initial $ server {state = c.shp} (handleRequest requestHandler)
