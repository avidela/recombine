module Server.Engine

import Data.IORef
import Data.Product
import Data.IO.Logging

import Server.Server
import Requests

%default total

public export
ServerM : Type -> Type
ServerM = Either ServerError

-- A Handler attemps to parse the request and uses the current state to compute a response
public export
Handler : Type -> Type
Handler state = Request -> LogIO state (ServerM String)

-- Here the handlers are both parsing the string and returning the result of
-- the computation as an encoded string. This is to avoid leaking the detail of
-- the dependency between the parsed type and the type of the handler.
export partial
server : {0 state : Type} -> (handlers : Handler state) -> LogIO state ()
server handler = Logging.do
    logVerbose "Server waiting for requests"
    req <- awaitRequest
    logVerbose ("successfully parsed request:")
    logVerbose ("----------------------------")
    logVerbose (show req)
    logVerbose ("----------------------------")
    result <- handler req
    either (logError . show) sendRequest result
    server handler

export
handleRequest :
    ReqHandler state ->
    Handler state
handleRequest (MkReqHandler costate args router idx reqBdy compIdx bodyParser resp handle
                        update serialise)
              (MkReq method version headers path body) =
  let Right (args, _) = parse router (path ++ "/" ++ show method)
        | Left err => logError "could not parse path \{path}"
                   *> pure (Left $ ParseError err)
      oldState = !getState
      Right (body, _) = parse (bodyParser {i = compIdx (oldState && args)}) body
        | Left err => logError "could not parse body \{body}"
                   *> pure (Left $ ParseError err)
      (newState && response) = handle (oldState && args) body
   in writeState (update oldState newState)
   *> pure (Right (serialise response))

