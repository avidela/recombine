||| Test server that takes a list of commands from CLI and executes them
module Server.Test

import Server
import Data.Container
import System
import System.File
import Data.String
import Requests

||| runServer for testing takes a filename as argument which contains a list of
||| commands to execute.
covering export
runServer : {0 c : Container} -> ServerInstance api c =>
            api -> LogLevel -> (initial : c.shp) -> IO ()
runServer api log initial = do
  [_, filename] <- getArgs
    | v => putStrLn "expected 1 argument: the file name. Got \{show $ length v `minus` 1} arguments instead"
  Right fileContent <- readFile filename
    | Left err => putStrLn "error while reading the file: \{show err}"
  let commands = lines fileContent
  runLogFile {lvl = log} initial (server (handleRequest (toHandler api))) commands
