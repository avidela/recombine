module Server.Node

import public Node.HTTP.Server
import public Node.HTTP
import Node.Error

import TyTTP.Adapter.Node.HTTP
import TyTTP.HTTP
import TyTTP
import TyTTP.Core.Request

import Control.Monad.Trans

import Data.Buffer
import Data.Buffer.Ext
import Data.Container
import Data.IORef

import Server
import Requests

convertMethod : HTTP.Method -> Maybe Requests.Method
convertMethod GET = Just Requests.GET
convertMethod POST = Just Requests.POST
convertMethod PUT = Just Requests.PUT
convertMethod (OtherMethod "PATCH") = Just Requests.PATCH
convertMethod (OtherMethod "UPDATE") = Just Requests.UPDATE
convertMethod DELETE = Just Requests.DELETE
convertMethod _ = Nothing

parseVersion : Version -> Maybe (n ** m ** HTTPVersion n m)
parseVersion Version_1_0 = Just (1 ** 0 ** V1996)
parseVersion Version_1_1 = Just (1 ** 1 ** V1997)
parseVersion Version_2 = Just (2 ** 0 ** V2015)
parseVersion (OtherVersion x) = Nothing


||| This function takes the global state and the log level of the server, as well as the request handler
||| combuted from our lens DSL.
||| The request we recieve are then passed to the Handler which routes them and generates responses.
||| After that we send the response back to the client.
handleFromRecombine : IORef LogLevel -> IORef st -> ReqHandler st ->
  Context HTTP.Method String Version StringHeaders Status StringHeaders (Publisher IO NodeError Buffer) ()
  -> IO (Context HTTP.Method String Version StringHeaders Status StringHeaders (Publisher IO NodeError Buffer)
                                                                       (Publisher IO NodeError Buffer))
handleFromRecombine log stateRef handler ctx =
  let req = ctx.request
      p : Publisher IO NodeError Buffer = MkPublisher $ \s => do

      -- This IORef holds the request body
      buf <- newIORef (the (Maybe Buffer) Nothing)
      putStrLn "starting to read the request body"

      -- Asynchronously read the request body
      ctx.request.body.subscribe
        -- To read the body asynchronously we need to write a subscriber.
        -- Subscribers are in 3 parts:
        -- - What happens when we recieve new data
        -- - What happens when we're done
        -- - What happens in case of errors
        -- Because the first and second step need to communicate we need
        -- to rely on global state, hence the IORef
        (MkSubscriber
          -- overwrite the buffer if we find a request body
          { onNext = (writeIORef buf . Just)
          -- When we're done retrieving the request body, read the IORef
          -- and construct a request to pass to the handler
          , onSucceded = (\() => do
              let body = maybe "" show !(readIORef buf)
              putStrLn "finished reading the request body, found: \{body}"
              let Just method = convertMethod req.method
                | Nothing => putStrLn "Unhandled method \{show req.method}"
              putStrLn "parsing version \{show req.version}"
              let Just ver = parseVersion req.version
                | Nothing => putStrLn "unexpected version \{show req.version}"
              result <- runLogRef log stateRef (handleRequest handler
                (MkReq { method = method
                       , version = ver
                       , headers = TyTTP.Core.Request.Request.headers req
                       , path = Request.Request.url req
                       , body = body
                       }))
              putStrLn "sending response with body \{show result}"
              case result of
                   Left err => printLn err
                   Right val => s.onNext (fromString val) *> s.onSucceded ()
            )
          -- In case of errors, just print the error to stdout
          , onFailed = s.onFailed
          })
  in putStrLn "got a new request \{show req.method} \{show req.url}" *> pure ({ response.body := p} ctx)

Error ServerError where
  message = show

export
runNodeServer : {0 c : Container} -> ServerInstance api c =>
                api -> LogLevel -> (initial : c.shp) ->  IO (HTTP, Server)
runNodeServer api logLevel initial = do
  http <- require
  st <- newIORef initial
  log <- newIORef logLevel
  let handler = toHandler api
  putStrLn "starting server"
  server <- HTTP.listen' {e = ServerError} :> handleFromRecombine log st handler
  pure (http, server)
