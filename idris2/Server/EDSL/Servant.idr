module Server.EDSL.Servant

import Server
import Requests
import Data.List.Quantifiers
import Data.Product
import Data.Sum
import Data.String.Singleton

infixr 7 /
%hide Prelude.(/)

(/) : (a -> b) -> a -> b
(/) = ($)

data Method : Type where
  GET : Method
  POST : (body : Type) -> HasParser body => Documented body => Method

data URLPath : Type where
  Path : String -> URLPath -> URLPath
  Cap : (ty : Type) -> HasParser ty => Documented ty => URLPath -> URLPath
  Split : List URLPath -> URLPath
  End : Method -> (response, state : Type) ->
        Default state => Display state => Show state =>
        URLPath

API : URLPath -> Type
API (Path x y) = API y
API (Cap ty y) = ty -> API y
API (Split xs) = HList (map API xs)
API (End GET response state) = state -> response
API (End (POST body) response state) =  body -> state -> response

PathToComp : URLPath -> List PathComp
PathToComp (Path x y) = map (Tpe (Str x)) (PathToComp y)
PathToComp (Cap x y) = map (Tpe x) (PathToComp y)
PathToComp (Split xs) = xs >>= PathToComp
PathToComp (End GET response state) = [ End ]
PathToComp (End (POST body) response state) = [ Tpe body End ]

InputStates : URLPath -> Type
InputStates (Path x y) = InputStates y
InputStates (Cap x y) = InputStates y
InputStates (Split []) = Unit
InputStates (Split (x :: [])) = InputStates x
InputStates (Split (x :: (y :: xs))) = InputStates x * InputStates (Split (y :: xs))
InputStates (End x response state) = state

OutputStates : (p : URLPath) -> InputStates p -> Type
OutputStates (Path y z) x = OutputStates z x
OutputStates (Cap y z) x = OutputStates z x
OutputStates (Split []) x = Unit
OutputStates (Split (y :: [])) x = InputStates y
OutputStates (Split (y :: (v :: w))) (a && b) =
      OutputStates y a + OutputStates (Split (v :: w)) b
OutputStates (End y response state) x = state

toFullPaths : (m : URLPath) -> (acc : PathComp) -> List (FullPath (InputStates m) (const (InputStates m)))
toFullPaths (Path str ps) acc = toFullPaths ps (Tpe (Str str) acc)
toFullPaths (Cap ty ps) acc = toFullPaths ps (Tpe ty acc)
toFullPaths (Split []) acc = []
toFullPaths (Split (x :: [])) acc = toFullPaths x acc
toFullPaths (Split (x :: (y :: xs))) acc = let rec = toFullPaths x acc
                                               rec2 = toFullPaths (Split (y :: xs)) acc in
                                               ?toFullPaths_rhs_8
toFullPaths (End GET response state) acc = [ MkFull Nothing (reverse acc) (Query state) (const state) ]
toFullPaths (End (POST body) response state) acc =
  [ MkFull Nothing
           (reverse acc)
           (Update (reverse acc) state (const state) (const body) (const id))
           (const state) ]

{-
MyRoutes : URLPath
MyRoutes = Cap Int / Cap Int / Path "p" / Split [ Path "add" / End GET Int ()
                                     , Path "sub" / End GET Int ()
                                     , Path "div" / End GET Int ()
                                     ]

exampleServer : API MyRoutes
exampleServer x y = [ \() => x + y
                    , \() => x - y
                    , \() => x `div` y
                    ]


[servant] {0 st : Type} -> {m : URLPath} -> ServerInstance (API m) (InputStates m) (OutputStates m) where
  toPaths {m = (Path x y)} api = ?impl_0
  toPaths {m = (Cap x y)} api = ?impl_1
  toPaths {m = (Split xs)} api = ?impl_2
  toPaths {m = (End GET response state)} api =
    pure (MkFull Nothing End (Query state) (\_ => state) :|: ?impl)
  toPaths {m = (End (POST body) response state)} api =
    pure (MkFull Nothing End (Update End state (const state) (const response) ?up) ?res :|: ?dsaimpl)


{-

main : IO ()
main = runServer @{servant} Normal exampleServer ()
