module Server.EDSL.HandlerLens

import Optics.Dependent
import Server.Server
import Data.String.ParserInterface
import Data.String.Parser
import Data.Action

indexedParser : IHasParser r.shp r.pos => {i : r .shp} -> ParseT Identity (r .pos i)
indexedParser = iPartialParse {v=i} {idx = r.shp} {t = r.pos}

public export
[bidirectional]
    (parseURI : HasParser l.shp) =>
    (parseBody : IHasParser r.shp r.pos) =>
    (action : Action p) =>
    (serialise : IShow l.shp l.pos) =>
    ServerInstance (DepParLens l p r) p where
  toHandler (MkContMor get set) = MkReqHandler
    { costate = p.pos
    , arguments = l.shp
    , router = partialParse {t=l.shp}
    , idx = r.shp
    , requestBody = r.pos
    , computeIndex = get
    , bodyParser = indexedParser
    , response = l.pos
    , handle = set
    , update = iUpdate
    , serialise = iShow {idx = l.shp} {t=l.pos}
    }

