module Server.EDSL.Lens

import Server.Server
import Interfaces
import Data.String.Parser
import public Data.Action
import public Data.String.ParserInterface
import public Data.String.Singleton
import public Data.Product
import public Data.Carrier
import public Data.Container

import public Optics.Dependent
import public Optics.Ops

import Server.EDSL.HandlerLens
import public Server.EDSL.Path
import public Server.Path
import Debug.Trace

import Data.IO.Logging

import Data.String
import Text.PrettyPrint.Prettyprinter.Doc

%default total
%hide Prelude.(&&)
%hide Prelude.(*)
%hide Prelude.(+)

infixl 7 &&
-- External choice with common parameter operator
infixl 7 &&&
-- External choice operator
infixl 7 +&&&+
-- Post composition operator
infixl 8 >>>
-- Precomposition operator
infixr 8 <<<
-- Tensor operator
infixl 9 ***
infixr 9 /
infixr 9 :/

-----------------------------------------------------------------------------------------------------------------
-- Parsing interfaces:
-- Those interfaces are necessary to explain how we expect to parse incoming network requests. In particular they
-- explain how to combine endpoint parsers when endpoints are combined together.
-----------------------------------------------------------------------------------------------------------------


||| Prefix a lens with a `String` singleton, this is used to parse path components
export
prefixPath : (str : String) ->
             DepParLens l p r ->
             DepParLens (MkCont (Str str +/ l.shp) (\x => l.pos x.tail)) p r
prefixPath str = preCompose (MkContMor ((.tail) . π2) (\(() && x) => (() &&)))

export
prefixType : (ty : Type) ->
             DepParLens l p r ->
             DepParLens (MkCont (ty +/ l.shp) (\x => ty * l.pos x.tail)) p r
prefixType ty = preCompose (MkContMor ((.tail) . π2) (\(() && x) => (() &&) . (x.head &&)))

-----------------------------------------------------------------------------------------------------------------
-- The Embedded Domain Specific Language for servers as lenses
-----------------------------------------------------------------------------------------------------------------

||| The EDSL for servers as lenses, it's parameterised over the same types as lenses and provides
||| convenience constructors for prefixing a server with a path component (`/`), constructing a server
||| from a lens (`Lens`), and accessing the state of the server (`State`).
||| It supports an implementation of `ServerInstance` which allows any value of this datatype to be
||| used to instanciate a server
public export
data DepServer : (l, p, r : Container) -> Type where

  ||| A Lens as a server, the parameters `p` and `q` are the state before and after updating it,
  ||| `s` is the type if the content of the `GET` requests we expect, `a` is the return type of the `GET`
  ||| requests we expect. `s` is the input type of the `POST` requests we expect, and `t` is the return
  ||| type of the `POST` requests we expect to serve.
  ||| All input types must be `Parsable` so that we can parse them from a network request. They must also
  ||| be `Display` for documentation purposes. All return types must be `Show` so that we can serialise them
  ||| Into network requests.
  Lens : (sParser : HasParser l.shp) => (sShow : Show l.shp) =>
         (aShow : Show r.shp) =>
         (iShowST : IShow l.shp l.pos) =>
         (lens : DepParLens l p r) ->
         (iParserAB : IHasParser r.shp r.pos) =>
         (update : Action p) =>
         DepServer l p r
  ||| A convenience constructor to access the state of a server, note that this requires the
  ||| State to be parsable so that it can be overridden through a `POST` request.
  State : (0 st : Type) -> Show st =>
          -- The state here needs to be parsable because it assumes that we are going to update it
          -- by overriding it entirely through a network request. In reality this rarely happens
          -- Which means we need to find a way to only use the `get` part of the lens without the
          -- `update` part, but also we need to use the `update` part if we compose it with other
          -- lenses which themselves want to expose their `update` function
          HasParser st =>
          DepServer (Const Unit) (Const st) (Const st)

  Reparam : DepLens z p -> Action z => IShow z.shp z.pos =>
            DepServer l p r -> DepServer l z r

  ||| Prefix a server with a path component
  (/) : (str : String) ->
        DepServer l p r ->
        DepServer (MkCont (Str str +/ l.shp) (\x => l.pos x.tail)) p r

  (:/) : (ty : Type) ->
         Show ty => HasParser ty =>
         DepServer l p r ->
         DepServer (MkCont (ty +/ l.shp) (\x => ty * l.pos x.tail)) p r

  (***) : DepServer l p r
       -> DepServer x q y
       -> DepServer (l `×` x)
                    (p `×` q)
                    (r `×` y)

  ||| Choice of two servers. Typically used to combine multiple endpoints together
  (+&&&+) :
       DepServer l p r
    -> DepServer l' p' r'
    -> DepServer (l + l') (p * p') (r + r')
  ||| Choice of two servers with a common state. Used to combine multiple endpoints
  ||| together.
  (&&&) :
       DepServer l1 p r1
    -> DepServer l2 p r2
    -> DepServer  (l1 + l2) p (r1 + r2)

  ||| Postcompose a server with a lens, allows to change the return type and arguments of
  ||| an endpoint through a lens.
  (>>>) : Show r.shp => IHasParser r.shp r.pos =>
         DepServer l p m -> DepLens m r ->  DepServer l p r

  ||| Postcompose a server with a lens, allows to change the return type and arguments of
  ||| an endpoint through a lens.
  (<<<) : (show : Show l.shp) => (par : HasParser l.shp) =>
        (iShow : IShow l.shp l.pos) =>
        DepLens l m -> DepServer m p r -> DepServer l p r

||| A conversion function from the `DepServer` embedded DSL into a `DepParLens`
||| This function plays the role of providing the correct interface witnesses for
||| parsing and serialising data in network requests.
serverAsLens :
               DepServer l st r ->
               (lens : DepParLens l st r **
               ( HasParser l.shp
               * Show r.shp
               * IShow l.shp l.pos
               * IHasParser r.shp r.pos
               * Action st))
serverAsLens (Lens x) = (x
    ** (%search && %search && %search && %search && %search))
serverAsLens (State st) = (stateLens st
    ** (%search && %search && %search && %search && %search))
serverAsLens (str / x) = let (v ** (par && _ && _ && _ && _)) = serverAsLens x
                               in (prefixPath str v
   ** (parserPath && %search
       && funShow {b= _.pos} {f=(.tail)} && %search
       && %search ))
serverAsLens (ty :/ x) = let (v ** (_ && _ && _ && _ && _)) = serverAsLens x
                               in (prefixType ty v
    ** (parserPath
       && %search && ishowTy {q = _.pos}
       && %search && %search))
serverAsLens (x +&&&+ y)  = let (s1 ** (_ && _ && ishow1 && _ && parse1)) = serverAsLens x
                                (s2 ** (_ && _ && ishow2 && _ && parse2)) = serverAsLens y
                             in (extChoice s1 s2
    ** (%search && %search
     && ishowSumDep
     && iparserDepSum && prodAction @{parse1} @{parse2}))
serverAsLens (x &&& y)  = let (s1 ** (_ && _ && _ && _ && _)) = serverAsLens x
                              (s2 ** (_ && _ && _ && _ && iupdatePQ)) = serverAsLens y
                              in (cloneChoice s1 s2 **
   (%search && %search
  && ishowSumDep
  && iparserDepSum && iupdatePQ ))
serverAsLens (ls >>> ks) = let (ls' ** (_ && _ && _ && _ && _ )) = serverAsLens ls in
                             (Dependent.postCompose ls' ks
    ** (%search && %search
       && %search && %search && %search
       ))

serverAsLens (ks <<< ls) = let (ls' ** (_ && _ && _ && _ && _ )) = serverAsLens ls
                            in (Dependent.preCompose ks ls'
    ** (%search && %search && %search
       && %search && %search
       ))
serverAsLens (s1 *** s2) = let (k1 ** (_ && _ && is1 && ip1 && iup1)) = serverAsLens s1
                               (k2 ** (_ && _ && is2 && ip2 && iup2)) = serverAsLens s2 in
                               (Dependent.parallel k1 k2 **
                               (%search && %search &&
                                ishowTensorProd @{is1} @{is2}  &&
                                iparserDepProd @{ip1} @{ip2} && tensorAction @{iup1} @{iup2}))
serverAsLens (Reparam rep ser) =
  let (ls ** (_ && _ && _ && _ && _)) = serverAsLens ser
  in (Dependent.reparam ls rep **
     (%search && %search && %search && %search && %search ))

export
record GetWrapper (t : Type) where
  constructor MkGetWrapper
  unwrap : t

export
HasParser t => HasParser (GetWrapper t) where
  partialParse = map MkGetWrapper (partialParse {t} <* string "/GET")

export
Show t => Show (GetWrapper t) where
  show = show . unwrap

export
record PostWrapper (t : Type) where
  constructor MkPostWrapper
  unwrap : t

export
Show t => Show (PostWrapper t) where
  show = show . unwrap

export
HasParser t => HasParser (PostWrapper t) where
  partialParse = map MkPostWrapper (partialParse {t} <* string "/POST")

||| Convert a lens into a server implementation, the server implementation exposes POST and GET endpoints
||| The `GET` requests are built using the `view` part of the lense and the `POST` requests are handled
||| with the `update` part of the lens.
||| Tough this looks like it's only returning two endpoints, each lens is able to handle multiple endpoints
||| Assuming it is built using external choice (`extChoice`).
||| The `reparam` argument is a function that explains how to update the state given dependent substate of itself
||| All the other arguments are interface requirements that allow us to parse, serialise and display documentation
||| and debug information for the given endpoint.
fromLens : {0 l, p, r : Container} ->
           (sParser : HasParser l.shp) =>
           (rParser : IHasParser r.shp r.pos) =>
           (aShow : Show r.shp) =>
           (tShow : IShow l.shp l.pos) =>
           -- A reparameterization function in order to update the overall state from the sub-state
           (reparam : (state : p.shp) -> p.pos state -> p.shp) ->
           -- The server as a lens
           (server : DepParLens l p r) ->
           (DepParLens (MkCont (GetWrapper l.shp) (const r.shp)) (MkCont p.shp (Const ())) CUnit
          , DepParLens (MkCont (PostWrapper l.shp) (\x => l.pos x.unwrap)) p r)
fromLens reparam (MkContMor g s) =
  ( MkContMor (const ()) (\arg, () => () && g (mapSnd unwrap arg))
  , MkContMor (\x => x.π2.unwrap) (\x => (() &&)) `preCompose` MkContMor g s)

serialiseResponse : Show r.shp => IShow l.shp l.pos =>
  IShow (GetWrapper (l .shp) + PostWrapper (l .shp))
        (choice (\value => r .shp) (\x => l .pos (x .unwrap)))
serialiseResponse = MkIShow showFn
  where
    showFn : {v : GetWrapper (l .shp) + PostWrapper (l .shp)} -> choice (\value => r .shp) (\x => l .pos (x .unwrap)) v -> String
    showFn {v = (<+ y)} x = show x
    showFn {v = (+> (MkPostWrapper unwrap))} x = iShow {t=l.pos} {v=unwrap} x

||| Convert a resource lens into a bidirectional lens. The composition rules for server become slightly different
||| so there is no way to convert back into a directional lens afterwards. This function also returns all the
||| evidence necessary to run it as a server.
export
resourceToBidirectional :
    -- The server
    DepServer l p r ->
    -- The lens we return, the top left boundary remains the same but we wrap it in
    -- `GetWrapper` and `PostWrapper` to parse both GET and POST requests for the same resource
    -- the state is a `Const p.shp` which relies on reparametrisation, the right boundary
    -- is the trickiest one because it is `()` for the `GET` lens and `r.shp` for the POST lens.
    -- This means that the bottom right boundary needs to remain `r.pos` for the POST lens, ideally
    -- we should be able to perform the parsing of the request body during routing but that is not
    -- the case here.
    (DepParLens (MkCont (GetWrapper (l.shp) + PostWrapper (l .shp))
                        (choice (const r.shp) (\x => l .pos (x .unwrap))))
               (Const p.shp)
               (MkCont (() + r .shp) (choice (const ()) r .pos))
    , IHasParser r.shp r.pos * Show r.shp * IShow l.shp l.pos * HasParser (l .shp))
resourceToBidirectional s =
    let (lens ** (_ && _ && _ && _ && up)) = serverAsLens s
        (getLens, postLens) = fromLens iUpdate lens
    in (reparam {p' = Const p.shp}
               (getLens `extChoice` postLens)
               (MkContMor (\x => dup x.π2)
                          (\x => \case (<+ y) => x
                                       (+> y) => () && iUpdate @{up} x.π2 y))
       , %search)


||| Allows to instanciate a server from a value of `DepServer`
-- The implementation amounts to convert the server into a `DepParLens` using `serverAsLens`, this function
-- also returns all the necessary interface instances to convert the lens into a server using `fromLens`
export
implementation {l : Container} -> {p : Container} -> {r : Container} ->
               ServerInstance (DepServer l p r) p where
  toHandler s = let (newLens, _ && _ && _ && _) = resourceToBidirectional s
                 in toHandler @{bidirectional {parseURI = %search} {parseBody = iparserDepSum}
                                              {action = %search} {serialise = serialiseResponse}} newLens


public export
Pure : HasParser a => Show a => Show b =>
            (a -> b) -> DepServer (MkCont a (const ())) CUnit (MkCont b (const ()))
Pure f = Lens (MkLens f (const (const ())))

||| Generate a lens that convert from one path representation to another, useful to
||| write endpoints using the `DSLPath` type
public export
Path : (from : DSLPath) -> {0 t : PathType from -> Type} ->
       DepLens (MkCont (PathToParsable from) (\v => t (convert from v))) (MkCont (PathType from) t)
Path from = MkContMor (\(st && v) => convert from v) (\(st && v)=> (() &&))

||| Automatically extends a path by precomposing an adapter lens
public export
MkPath : (from : DSLPath) ->
         (parser : HasParser (PathToParsable from)) =>
         (show : Show (PathToParsable from)) =>
         {0 t : PathType from -> Type} ->
         DepServer (MkCont (PathType from) t) p r ->
         DepServer (MkCont (PathToParsable from) (\v => t (convert from v))) p r
MkPath path server =
  let (_ ** (c1 && f1 && b4 && d3 && e3)) = serverAsLens server in
      (<<<) {show = show}
            {iShow = funShow {s = b4}}
            {par = parser} (Path path) server

