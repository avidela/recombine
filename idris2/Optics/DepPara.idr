module Optics.DepPara

import Optics.Dependent

public export
DepLens : (p, q : Type)
       -> (a : Type) -> (b : a -> Type)
       -> (s : Type) -> (t : s -> Type) -> Type
DepLens p q a b s t = DepParLens p (\_ => q) a b s t

