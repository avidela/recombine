||| DepParLens :     Dependent parameters &     dependent arguments
||| -          :     Dependent parameters & non-dependent arguments
||| DepPara    : Non-dependent parameters &     dependent arguments
|||    Para    : Non-dependent parameters & non-dependent arguments
||| DepLens    :            No parameters &     dependent arguments
|||    Lens    :            No parameters & non-dependent arguments
module Optics.Dependent

import public Data.Product
import public Data.Boundary
import public Data.Sum
import public Data.Alg
import public Data.Container
import Control.Function
import Control.Relation

%default total

||| Dependent lenses with dependent parameters
public export
record DepParLens (l, p, r : Container) where
  constructor MkContMor
  get : p.shp * l.shp -> r.shp
  set : (x : p.shp * l.shp) -> r.pos (get x) -> p.pos x.π1 * l.pos x.π2

public export
toContainer : Boundary -> Container
toContainer b = MkCont b.proj1 (const b.proj2)

public export
Const : a -> b -> a
Const v _ = v

||| update the parameter across a lens
export
reparam : DepParLens l p r
       -> DepParLens p' CUnit p
       -> DepParLens l p' r
reparam (MkContMor get1 set1) (MkContMor get2 set2) =
  MkContMor (\x => get1 (get2 (() && x.π1) && x.π2))
            (\x, b => let qt = set1 (get2 (() && x.π1) && x.π2) b
                          rs = set2 (() && x.π1) qt.π1
                      in rs.π2 && qt.π2)

||| Sequential composition of dependent lenses with parameters
||| The parameters get tensored to represent the fact that
||| we need to handle both of them at once, one for each lens
--
--            p × p'                   q × q'
--             │ │                      │ │
--       ┌───────────────────────────────────────┐
--       │     │ │                      │ │      │
--       │     │ ╰──────────────────╮   │ │      │
--       │     │     ┌──────────────────╯ │      │
--       │     │     ▲              │     ▲      │
--       │  ┌──┴─────┴───┐       ┌──┴─────┴───┐  │
--   a ──┤──┤            ├─> s ──┤            ├──├─> x
--       │  │            │       │            │  │
--   b <─┤──┤            ├── t <─┤            ├──├── y
--       │  └────────────┘       └────────────┘  │
--       └───────────────────────────────────────┘
export
compose : DepParLens l p m
       -> DepParLens m p' r
       -> DepParLens l (p `×` p')  r
compose (MkContMor get1 set1) (MkContMor get2 set2) =
  MkContMor (\x => get2 (x.π1.π2 && get1 (x.π1.π1 && x.π2)))
            composeSet
  where
    composeSet : (v : (p .shp * p'.shp) * l .shp)
              -> r.pos (get2 (v.π1.π2 && get1 (v.π1.π1 && v.π2)))
              -> (p.pos v.π1.π1 * p'.pos v.π1.π2) * l.pos v.π2
    composeSet x y with (get2 ((x .fst) .snd && get1 ((x .fst) .fst && x .snd))) proof p1
      composeSet x y | get2Res with (get1 ((x .fst) .fst && x .snd)) proof p2
        composeSet x y | get2Res | get1Res =
            let v2 = set2 (x.π1.π2 && get1Res) (rewrite p1 in y)
                v1 = set1 (x.π1.π1 && x.π2) (rewrite p2 in v2.π2)
            in ((v1.π1 && v2.π1) && v1.π2)

||| Composition of lenses with a constant parameters
export
composeClone : {0 p, q : Type}
  -> DepParLens l (MkCont p (Const q)) m
  -> DepParLens m (MkCont p (Const q)) r
  -> DepParLens l (MkCont p (Const q)) r
composeClone (MkContMor get1 set1) (MkContMor get2 set2) =
  MkContMor (\arg => get2 (arg.π1 && get1 arg))
            (\arg, b => let s1 = set2 (arg.π1 && get1 arg) b
                        in set1 arg s1.π2)

||| Dependent lenses without parameters
namespace DepLens

    ||| Dependent lenses alias
    public export
    DepLens : (c1, c2 : Container) -> Type
    DepLens c1 c2 = DepParLens c1 CUnit c2

    public export
    MkDepLens : {0 a : Type} -> {0 b : a -> Type}
             -> {0 s : Type} -> {0 t : s -> Type}
             -> (f : s -> a)
             -> ((x : s) -> b (f x) -> t x)
             -> DepLens (MkCont s t) (MkCont a b)
    MkDepLens f g = MkContMor (f . π2) (\(() && x) => (() &&) . g x)

    export
    composeDepLens : DepLens l x -> DepLens x r -> DepLens l r
    composeDepLens l1 l2 = reparam (Dependent.compose {p = CUnit} {p' = CUnit} l1 l2)
                                   (MkContMor id (const id))

    ||| Dependent lenses have a transitive closure
    public export
    Transitive Container DepLens where
      transitive {x=l} {y=m} {z=r} l1 l2 = reparam (Dependent.compose l1 l2) (MkContMor id (const id))

||| Dependent lenses with non-dependent parameters
namespace DepPara

    public export
    DepPara : (left : Container) -> (para : Boundary) -> (right : Container) -> Type
    DepPara l p r = DepParLens l (MkCont p.proj1 (const p.proj2)) r

    public export
    compose : DepPara l p m -> DepPara m q r -> DepPara l (p `cartesian` q) r
    compose l1 l2 {p = MkB p1 p2} {q = MkB q1 q2} =
        reparam (Dependent.compose l1 l2)
                (MkContMor (fromPair . snd) (const ((() &&) . toPair)))
    public export
    composeClone : DepPara l p m -> DepPara m p r -> DepPara l p r
    composeClone l1 l2 = Dependent.composeClone l1 l2

||| Non-dependent lenses with non-dependent parameters
namespace Para

    ||| Para lenses alias
    -- get : p * s -> a
    -- set : p * s -> b -> p * t
    public export
    Para : (l, p, r : Boundary) -> Type
    Para l p r = DepParLens (toContainer l) (toContainer p) (toContainer r)

    public export
    MkPara : (p * x -> y) -> (p * x -> r -> q * s) -> Para (MkB x s) (MkB p q) (MkB y r)
    MkPara g s = MkContMor g s

    public export
    compose : Para l p m -> Para m q r -> Para l (p `cartesian` q) r
    compose {p = MkB p1 p2}
            {q = MkB q1 q2} l1 l2 = reparam (Dependent.compose l1 l2)
                            (MkContMor (fromPair . (.π2)) (const ((() &&) . toPair)))

    public export
    parameter : Para BUnit p p
    parameter = MkContMor π1 (\x, b => b && ())

||| Non-dependent lenses without parameters
namespace Plain
    public export
    Lens : (l, r : Boundary) -> Type
    Lens l r = DepLens (toContainer l) (toContainer r)

    public export
    MkLens : (s -> a) -> (s -> b -> t) -> Lens (MkB s t) (MkB a b)
    MkLens f g = MkContMor (f . π2) ((() &&) .: g . π2)

    export
    Transitive Boundary Lens where
      transitive {x} {y} {z} l1 l2 = reparam (Dependent.compose l1 l2) (MkContMor id (const id))

--
--          p     q                      s     t
--          ╷     ▲                      ╷     ▲
--       ┌──┴─────┴───┐               ┌──┴─────┴───┐
--   s ──┤            ├─> a       p ──┤            ├─> a
--       │            │      ==>      │            │
--   t <─┤            ├── b       q <─┤            ├── b
--       └────────────┘               └────────────┘
--
public export
swapParameters : DepParLens a b x -> DepParLens b a x
swapParameters (MkContMor s g) = MkContMor
    (s . swap)
    (\(a && b) => swap . (g (b && a)))

||| External choice of two lenses, picks one of the two lenses and runs it
||| The left boundaries are coproduct to indicate that we need to pick
||| from either the `ks` or `ls` lens. The parameters `p` are tensored to
||| indicate that we must be ready to handle either inputs. The parameters
||| `q` are co-product to indicate that once the update has been performed
||| only one the corresponding parameter from the lens `ks` or `ls` needs to
||| be updated
--
--                  p × p'                 q + q'
--                   │ │                    │ │
--       ┌───────────────────────────────────────────────────┐
--       │           │ ╰────────────────╮   │ │              │
--       │           │     ╭────────────│───╯ │              │
--       │           │     ▲            │     │              │
--       │        ┌──┴─────┴───┐        │     │              │
--       │  ╭─╴s╶─┤            ├─> a ───│─────│──────────╮   │
--       │  │     │     ks     │        │     │          │   │
--       │  │╭╴t<─┤            ├── b ───│─────│────────────╮ │
-- s+s' ═╡══╡│    └────────────┘        │     │          ╞═│═╞═> a+a'
--       │  ││                          │     ▲          │ │ │
-- t+t'<═╡══│╡                       ┌──┴─────┴───┐      │ ╞═╞══ b+b'
--       │  ╰────────────────── s'╶──┤            ├─> a'╶╯ │ │
--       │   │                       │     ls     │        │ │
--       │   ╰───────────────── t' <─┤            ├──╴b'╶──╯ │
--       │                           └────────────┘          │
--       └───────────────────────────────────────────────────┘
public export
extChoice :
       DepParLens l p r
    -> DepParLens l' p' r'
    -> DepParLens (l + l') (p * p') (r + r')
extChoice (MkContMor get1 set1) (MkContMor get2 set2) =
  MkContMor (bimap get1 get2 . Alg.distributive)
      (\case ((x1 && x2) && (<+ x)) => mapFst (<+) . set1 (x1 && x)
             ((x1 && x2) && (+> x)) => mapFst (+>) . set2 (x2 && x))

||| External choice of two lenses with a common parameter.
||| Simply implemented as external choice with diagonal reparametrisation.
export
cloneChoice :
       DepParLens l p r
    -> DepParLens l' p r'
    -> DepParLens (l + l') p (r + r')
cloneChoice c1 c2 = reparam (extChoice c1 c2)
                            (MkContMor (dup . π2)
                                       (\x => (() &&) . dia))

||| Tensor of two lenses, runs both of them at the same time
--
--                  p × p'                    q × q'
--                   │ │                       │ │
--       ┌──────────────────────────────────────────────────────┐
--       │           │ ╰───────────────────╮   │ │              │
--       │           │     ╭───────────────────╯ │              │
--       │           │     ▲               │     │              │
--       │        ┌──┴─────┴───┐           │     │              │
--       │  ╭─╴s╶─┤            ├─> a ───────────────────────╮   │
--       │  │     │            │           │     │          │   │
--       │  │╭╴t<─┤            ├── b ──────│─────│────────────╮ │
-- s×s' ═╡══╡│    └────────────┘           │     │          ╞═│═╞═> a×a'
--       │  ││                             │     │          │ │ │
-- t×t'<═╡══│╡                             │     ▲          │ ╞═╞══ b×b'
--       │  ││                          ┌──┴─────┴───┐      │ │ │
--       │  ╰───────────────────── s'╶──┤            ├─> a'╶╯ │ │
--       │   │                          │            │        │ │
--       │   ╰──────────────────── t' <─┤            ├──╴b'╶──╯ │
--       │                              └────────────┘          │
--       └──────────────────────────────────────────────────────┘
export
parallel : DepParLens l p r
      -> DepParLens l' p' r'
      -> DepParLens (l `×` l') (p `×` p') (r `×` r')
parallel (MkContMor get1 set1) (MkContMor get2 set2) =
  MkContMor (elim get1 get2 (&&) . shuffle)
            (\((p1 && p2) && (x1 && x2)) => shuffle . bimap (set1 (p1 && x1)) (set2 (p2 && x2)))

||| If the right side is tensored with a unit, we remove the unit
export
removeUnitRight : DepLens l (l `×` CUnit)
removeUnitRight = MkContMor swap ((\(x1 && x2) => swap))

export
removeUnitRight' : DepLens l (CUnit `×` l)
removeUnitRight' = MkContMor id (\x => id)

||| If the left side is tensored with a unit we remove the unit
export
removeUnitLeft : DepLens (r `×` CUnit) r
removeUnitLeft = MkContMor (π1 . π2) (\(() && x1 && x2) => (() &&) . (&& ()))

||| If the left side is tensored with a unit we remove the unit
export
removeUnitLeft' : DepLens (CUnit `×` r) r
removeUnitLeft' = MkContMor (π2 . π2) (\(() && () && x) => (() &&) . (() &&))

||| Tensor a lens with parameters with a lens without parameters on the right
export
tensorLensRight : DepParLens l p r
      -> DepLens x y
      -> DepParLens (l `×` x) p (r `×` y)
tensorLensRight l1 l2 = reparam (parallel l1 l2) removeUnitRight

||| Tensor a lens with parameters with a lens without parameters on the left
export
tensorLensLeft : DepLens x y
      -> DepParLens l p r
      -> DepParLens (x `×` l) p (y `×` r)
tensorLensLeft l1 l2 = reparam (parallel l1 l2) removeUnitRight'

||| update the left side across a depedent para lens
-- Given `ks` and `ls`:
--
--                                    p     q
--                                    ╷     ▲
--       ┌────────────┐            ┌──┴─────┴───┐
--   x ──┤            ├─> s    s ──┤            ├─> a
--       │     ks     │            │     ls     │
--   y <─┤            ├── t    t <─┤            ├── b
--       └────────────┘            └────────────┘
--
-- we obtain:
--
--                                 p     q
--                                 ╷     ▲
--       ┌─────────────────────────┴─────┴─────┐
--       │ ┌────────────┐       ┌──┴─────┴───┐ │
--   x ──┤─┤            ├─> s ──┤            ├─├─> a
--       │ │     ls     │       │     ks     │ │
--   y <─┤─┤            ├── t <─┤            ├─├── b
--       │ └────────────┘       └────────────┘ │
--       └─────────────────────────────────────┘
--
export
preCompose : DepLens l m -> DepParLens m p r -> DepParLens l p r
preCompose c1 c2 = reparam (compose c1 c2) removeUnitRight'


||| update the right side across a dependent para lens
--  given `ls` and `ks`:
--
--          p     q
--          ╷     ▲
--       ┌──┴─────┴───┐            ┌────────────┐
--   s ──┤            ├─> a    a ──┤            ├─> x
--       │     ls     │            │     ks     │
--   t <─┤            ├── b    b <─┤            ├── y
--       └────────────┘            └────────────┘
--
--  we obtain:
--
--            p     q
--            ╷     ▲
--       ┌────┴─────┴──────────────────────────┐
--       │ ┌──┴─────┴───┐       ┌────────────┐ │
--   s ──┤─┤            ├─> a ──┤            ├─├─> x
--       │ │     ls     │       │     ks     │ │
--   t <─┤─┤            ├── b <─┤            ├─├─ y
--       │ └────────────┘       └────────────┘ │
--       └─────────────────────────────────────┘
--
export
postCompose : DepParLens l p m -> DepLens m r -> DepParLens l p r
postCompose c1 c2 = reparam (compose c1 c2) removeUnitRight

||| Change the top and the left boundary of a dependent parameterised lens
export
composeInput : DepLens l' l
            -> DepLens p' p
            -> DepParLens l p r
            -> DepParLens l' p' r
composeInput left top ls = reparam (preCompose left ls) top

||| Tensor of lenses with a common parameter, requies an indexed monoid over `p` in order to combine the states
export
monoTensor : {prf : (v : p) -> Monoid (q v)}
          -> DepParLens l (MkCont p q) r
          -> DepParLens l' (MkCont p q) r'
          -> DepParLens (l `×` l') (MkCont p q) (r `×` r')
monoTensor v u = reparam (parallel v u)
  (MkDepLens dup (\x => let mon = prf x in uncurry (<+>)))

||| Extract and update the second element of a pair
export
fstLens : Lens (MkB (a * b) (a * b)) (MkB a a)
fstLens = MkLens fst (flip (\x => mapFst (const x)))

||| Extract and update the second element of a pair
export
sndLens : Lens (MkB (a * b) (a * b)) (MkB b b)
sndLens = MkLens snd (flip (\x => mapSnd (const x)))

||| The identity lens does nothing but carries along it arguments
public export
idLens : DepLens a a
idLens {a = MkCont a1 a2} = MkDepLens id (\x => id)

||| An adapter with identities on both parts
public export
idAdapter : (0 a, b : Type) -> DepParLens (MkCont a (const b)) CUnit (MkCont a (const b))
idAdapter a b = MkDepLens id (const id)

||| make a copy of an extra argument on the forward part of the lens
public export
parallelFwd : (0 a : Type) -> DepParLens (MkCont x s) p (MkCont y r)
                         -> DepParLens (MkCont (a * x) (s . (.π2)))
                                       p
                                       (MkCont (a * y) (r . (.π2)))
parallelFwd a lens = let para'd = idAdapter a Unit `parallel` lens
                         repa'd = reparam para'd (MkContMor id (\x => id))
                      in MkDepLens id (\x => π2) `preCompose` (repa'd `postCompose` MkDepLens id (\x => (() &&)))

||| Extract the parameter out and override it
--                   p     p
--                   ╷     ▲
--                ┌──┴─────┴───┐
--            x ──┤  └─────│───├─> p
--   p   ->       │        │   │
--            x <─┤        └───├── p
--                └────────────┘
--
export
stateLens : (0 p : Type) -> Para BUnit (Dup p) (Dup p)
stateLens p = MkContMor π1 (\(x1 && x2) => (&& x2))

export
parameter : (0 p : Boundary) -> Para BUnit p p
parameter p = MkContMor fst (const (&& ()))

export
viewParameter : (p : Type) -> Para BUnit (MkB p Unit) (MkB p Unit)
viewParameter p = MkPara π1 (const dup)
