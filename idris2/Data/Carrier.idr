module Data.Carrier


||| A wrapper around `Type`, specifically used for url captures
public export
data Carrier : Type -> Type where
  Carry : ty -> Carrier ty

||| Extract the values from a Carrier
public export
(.value) : Carrier ty -> ty
(.value) (Carry t) = t

||| Inherite the Show instance from the wrapped type
export
Show t => Show (Carrier t) where
  show v = show v.value

