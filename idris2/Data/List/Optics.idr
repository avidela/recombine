module Data.List.Optics

import Data.List
import Optics
import Data.Sum
import Data.Product

export
matchHead : Prism a a (List a) (List a)
matchHead = MkPrism (\case [] => <+ []; (x :: xs) => +> x)
                    (\x => [x])

matchTail : Prism (List a) (List a) (List a) (List a)
matchTail = MkPrism ?matchTail_rhs (\ls => ?wahu)

||| Get the list or prepend one element
export
updateHead : Lens (List a) a (List a) (List a)
updateHead = MkLens id (\(xs && x) => x :: xs)

updateSecond : Cartesian p => CoCartesian p => Optic p (List a) a (List a) (List a)
updateSecond = prism2Pro matchTail . lens2Pro updateHead
