module Data.Request

import Data.Product
import Data.String.ParserInterface
import Requests
import Data.String.Parser

%default total

export
interface RequestParser t where
  parseURI : Parser t
  parseMethod : Method -> Bool

public export
data GetRequest = Get

public export
data PostRequest = Post

public export
HasParser GetRequest where
  partialParse = (string "GET" <* eos) *> pure Get

public export
HasParser PostRequest where
  partialParse = (string "POST" <* eos) *> pure Post

