module Data.JSON.Record

infix 3 :!:
infix 8 :-:, :!

-- A Record Field is an instance of a field which requries the
-- name of the field and a value that inhabits the type of the field
public export
data RecordField : (field : String) -> (ty : Type) -> Type where
  (:-:) : (s : String) -> (_ : ty) -> RecordField s ty

||| A field type describe a record field by its name and its type
public export
record FieldType where
  constructor (:!)
  name : String
  type : Type

||| A record indexed by its fields, where each Field is a name and a type
public export
data Record : List FieldType -> Type where
  Nil : Record []
  (::) : RecordField s t -> Record ts -> Record ((s :! t) :: ts)

||| A proof that a record contains a certain field name
public export
data HasField : String -> List FieldType -> Type where
  Here : HasField f (f :! ty :: fs)
  There : HasField f fs -> HasField f (f' :: fs)

||| Return the type of a field inside a record
public export
GetType : {fs : _} -> HasField f fs -> Type
GetType Here {fs = f :! ty :: fs} = ty
GetType (There x) = GetType x

||| Project function takes a string and projects out the field with the corresponding name
public export
(.p) :  Record fs -> (f : String) -> {auto inField : HasField f fs} -> GetType inField
((f :-: val) :: y).p f {inField = Here} = val
(x :: xs).p f {inField = (There y)} = xs.p f {inField = y}

||| Modify a record field
public export
(.m) : Record fs -> (f : String) -> {auto inField : HasField f fs} -> GetType inField -> Record fs
(x :: f).m field newVal {inField = Here} = _ :-: newVal :: f
(x :: f).m field newVal {inField = (There fs)} = x :: f.m field newVal {inField = fs}



