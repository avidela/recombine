module Data.JSON.Lens

import Data.JSON.Record
import Optics.Dependent

public export
fieldLens : (f : String) -> {auto inField : HasField f fs} ->
          DepLens (Const (Record fs)) (Const (GetType inField))
fieldLens f = MkContMor (\x => x.π2.p f) (\x, newVal => () && x.π2.m f newVal)

public export
fieldLens' : (f : String) -> {0 from : Type} -> {auto inField : HasField f fs} ->
            {auto 0 fromIsRecord : from === Record fs} ->
            DepLens (Const (Record fs)) (Const (GetType inField))
fieldLens' f = MkContMor (\x => x.π2.p f) (\x, newVal => () && x.π2.m f newVal)
