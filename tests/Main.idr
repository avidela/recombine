module Main

import Test.Golden

node : TestPool
node = MkTestPool "node tests" [] Nothing [ "node/echo" ]

cli : TestPool
cli = MkTestPool "cli test" [] Nothing [ "cli/combined" ]

main : IO ()
main = runner [ node, cli ]
