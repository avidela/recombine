module Echo

import Node
import Node.HTTP.Client
import Node.HTTP.Server
import TyTTP.Adapter.Node.HTTP as HTTP
import TyTTP.HTTP
import Data.Buffer
import Data.Container
import Server
import Server.Node
import Server.EDSL.Lens

echoServer : DepServer (MkCont (Unit * Unit) (const String)) CUnit (MkCont String (const String))
echoServer = Lens (MkContMor (\x => "GET") (\x => \msg => () && "POST: \{msg}"))

main : IO ()
main = do
  putStrLn "hello1"
  (http, server) <- runNodeServer echoServer Normal ()
  putStrLn "hello"

  defer $ do
    ignore $ http.get "http://localhost:3000" $ \res => do
      putStrLn "GET"
      printLn res.statusCode
      onData res putStrLn

  defer $ do
    clientReq <- http.post "http://localhost:3000" $ \res => do
      putStrLn "POST"
      printLn res.statusCode
      onData res putStrLn
      server.close

    clientReq.write "Hello World!"
    clientReq.write "With more chunks"
    clientReq.end
