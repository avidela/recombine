# The Recombine library

Recombine is a library based on _lenses_ to describe and implement web servers.

In Recombine, each endpoint is both _described_ and _implemented_ with a lens.
A Lens is a datastructure allowing accessing and updating other datastructures, in
our case, we are accessing and updating the state of the server.

With the library comes a Domain Specific Language (DSL) that allows you to easity
write server descriptions, servers are implemented by composing multiple endpoints
together, the resulting server can also be composed with other lenses, rendering
the entire stack composable.

The DSL allows you to use familiar syntax in order to compose lenses and declare
endpoints:

```
import Server
import Server.EDSL.Lens

myServer : ?
myServer = "lights" / ("living" / Lens livingroomLight
                       &&&
                       "bedroom" / Lens bedroomLight
                       &&&
                       "kitchen" / Lens kitchenLight)
           &&&
           "boiler" / Lens serverBoilerLens

main : IO ()
main = runServer Normal myServer def
```

The `&&&` operator allows to combine multiple endpoints together to build a server.
The `/` operator allows you to add path components to parse before handling a request.

## Installation

```
> idris2 --install recombine.ipkg
```

Once the library is installed you can compile the examples by going into the examples directory
`cd examples/` and running

```
> idris2 --o main iot.ipkg
> ./build/exec/main
```

This will run a mock server that read a request on stdin and print the result on stdout.

## Video presentations

- Boilerplate-free server using lenses, MSP 101, March 2022: https://youtu.be/GkYBWxT7RAU
- Parameterised lenses & client-server relationships, SPLS, October 2021: https://youtu.be/eeVYmtjAMSc
- Optics for servers, July 2021: https://youtu.be/4xpbYPa1lTc
