<!---
This template is for Merge Resquests fixing a particular issue.
Those could be bug fixes, new features or refactorings.
Tell which issue it is you are fixing using `fix #*issue number*
and describe the steps you took.

If you are creating a Merge Request for a bugfix that does not
have an associated issue, select the template "Bug fix without issue"
-->

fix #<!--the issue you are fixing-->

## Description

<!---
Tell what you had to do in order to fix the issue, try to help the reviewers
look for some particular pattern or point out things that stood out to you
while implementing the MR
-->

/assign @avidela

